Public Class Game : Implements onRecieveDataListener
    Private Const intGrow As Integer = 3
    Private Const intWidth As Integer = 8
    Private cSnake1 As Snake
    Private cSnake2 As Snake
    Private cMovement1 As Movement
    Private cMovement2 As Movement

    Private blnMoving As Boolean = False
    Private blnExpanding1 As Boolean = False
    Private blnExpanding2 As Boolean = False

    Private rectFood As Rectangle
    Private intScore1, intScore2 As Integer

    Private Score1 As String
    Private Score2 As String
    Private isDead As Boolean = True
    Private player1_color As Brush
    Private player2_color As Brush

    Public Sub Feed()
        Dim pntFood As Point
        Do
            pntFood = Randomize()
            If Not (cSnake1 Is Nothing) Then
                If Not cSnake1.FoodPlacedOnSnake(pntFood) Then Exit Do
            Else
                Exit Do
            End If
            ' snake 2
            If Not (cSnake2 Is Nothing) Then
                If Not cSnake2.FoodPlacedOnSnake(pntFood) Then Exit Do
            Else
                Exit Do
            End If
        Loop
        rectFood.Location = pntFood
    End Sub

    Private Sub Die()
        isDead = True
        DisplayMessage("Press Enter to play or Escape to quit.")
        Initialize()
    End Sub


    Private Sub Initialize()
        intScore1 = 0
        intScore2 = 0

        rectFood = New Rectangle(0, 0, intWidth, intWidth)
        Feed()
        Dim pntStart As New Point(CInt(surfaceSnake.ClientSize.Width / 2 / intWidth + 0.5) * intWidth, CInt(surfaceSnake.ClientSize.Height / 2 / intWidth + 0.5) * intWidth)

        cSnake1 = New Snake(pntStart, intWidth, 1)
        cSnake2 = New Snake(pntStart, intWidth, 1)
        cMovement1 = New Movement(intWidth, cSnake1.Head.Loc, Movement.intDirection.Right)
        cMovement2 = New Movement(intWidth, cSnake2.Head.Loc, Movement.intDirection.Left)

        blnExpanding1 = True
        blnExpanding2 = True
        isDead = True

    End Sub

    Private Sub UpdateUI()
        Static iGrow1 As Integer = intGrow
        Static iGrow2 As Integer = intGrow

        Static intAddSeg1 As Integer
        Static intAddSeg2 As Integer

        If Not blnMoving Then Exit Sub

        cMovement1.Move(surfaceSnake.ClientRectangle)
        cMovement2.Move(surfaceSnake.ClientRectangle)

        'If cSnake1.FoodPlacedOnSnake(cMovement1.Location) Then
        'iGrow1 = 0
        'intAddSeg1 = 0
        ' Die()
        'Return
        ' Else
            If rectFood.Contains(cMovement1.Location) Then
                iGrow1 += intGrow
                blnExpanding1 = True
                Feed()
                intScore1 += 5
            Label1.Text = "" + intScore1.ToString
            End If
            If blnExpanding1 Then
                If iGrow1 < intGrow Then iGrow1 = intGrow
                If intAddSeg1 >= iGrow1 Then
                    blnExpanding1 = False
                    intAddSeg1 = 0
                    iGrow1 = 0
                    cSnake1.Move(cMovement1.Location)
                Else
                    cSnake1.Eat(cMovement1.Location)
                    intAddSeg1 += 1
                End If
            Else
                cSnake1.Move(cMovement1.Location)
            End If

            ' snake 2
        'If cSnake2.FoodPlacedOnSnake(cMovement2.Location) Then
        'iGrow2 = 0
        'intAddSeg2 = 0
        'Die()
        'Return
        'Else
            If rectFood.Contains(cMovement2.Location) Then

                iGrow2 += intGrow
                blnExpanding2 = True
                Feed()
                intScore2 += 5
                Label2.Text = "" + intScore2.ToString
            End If
            If blnExpanding2 Then
                If iGrow2 < intGrow Then iGrow2 = intGrow
                If intAddSeg2 >= iGrow2 Then
                    blnExpanding2 = False
                    intAddSeg2 = 0
                    iGrow2 = 0
                    cSnake2.Move(cMovement2.Location)
                Else
                    cSnake2.Eat(cMovement2.Location)
                    intAddSeg2 += 1
                End If
            Else
                cSnake2.Move(cMovement2.Location)
            End If

    End Sub

    Private Sub DisplayMessage(ByVal strMsg As String)

        lblMessage.Text = strMsg
        lblMessage.Visible = True
        blnMoving = False
        surfaceSnake.Enabled = False

    End Sub

    Public Function Randomize() As Point

        Dim rnd As New Random(Now.Second)

        Dim intScreenWidth As Integer = ((surfaceSnake.ClientRectangle.Width \ intWidth) - 4) * intWidth
        Dim intScreenHeight As Integer = ((surfaceSnake.ClientRectangle.Height \ intWidth) - 4) * intWidth

        Dim intX As Integer = rnd.Next(2, intScreenWidth)
        Dim intY As Integer = rnd.Next(2, intScreenHeight)

        intX = (intX \ intWidth) * intWidth
        intY = (intY \ intWidth) * intWidth

        Return New Point(intX, intY)

    End Function

    Private Sub HideMessage()
        ' set scores
        lblMessage.Visible = False
        blnMoving = True
        Timer1.Enabled = True
        If My.Settings.isP1_computer Then
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub surfaceSnake_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles surfaceSnake.Paint

        Dim g As Graphics = e.Graphics
        Dim numOfCells As Integer = 200
        Dim cellSize As Integer = 8
        Dim p As Pen = New Pen(Color.Black)

        For y As Integer = 0 To numOfCells - 1
            g.DrawLine(p, 0, y * cellSize, numOfCells * cellSize, y * cellSize)
        Next

        For x As Integer = 0 To numOfCells - 1
            g.DrawLine(p, x * cellSize, 0, x * cellSize, numOfCells * cellSize)
        Next

        If Not blnMoving Then
            e.Graphics.Clear(surfaceSnake.BackColor)
            Exit Sub
        End If
        'e.Graphics.FillEllipse(Brushes.White, rectFood)
        Dim segCurrent As Segment
        ' draw player 1
        player1_color = New SolidBrush(My.Settings.player1_color)
        For Each segCurrent In cSnake1.NumberOfSegments
            e.Graphics.FillRectangle(player1_color, segCurrent.Rect)
        Next
        'draw player 2
        player2_color = New SolidBrush(My.Settings.player2_color)
        For Each segCurrent In cSnake2.NumberOfSegments
            e.Graphics.FillRectangle(player2_color, segCurrent.Rect)
        Next
        e.Graphics.FillEllipse(Brushes.White, rectFood)

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        UpdateUI()
        surfaceSnake.Invalidate()
    End Sub

    Private Sub Game_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Form1.Show()
    End Sub

    Private Sub Game_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Select Case e.KeyCode
            Case Keys.Enter
                HideMessage()
                isDead = False
            Case Keys.Space
                If blnMoving Then
                    'DisplayMessage("Press Enter to continue or Escape to quit.")
                    Timer1.Stop()
                    Timer2.Stop()
                    isDead = False
                Else
                    Me.Close()
                    Timer1.Stop()
                    Timer2.Stop()
                    isDead = True
                End If
        End Select
    End Sub

    Private Sub Game_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If My.Settings.isP1_computer Then
            Select Case e.KeyCode
                Case Keys.Right
                    If Not cMovement2.Direction = Movement.intDirection.Left Then
                        cMovement2.Direction = Movement.intDirection.Right
                    End If
                Case Keys.Down
                    If Not cMovement2.Direction = Movement.intDirection.Up Then
                        cMovement2.Direction = Movement.intDirection.Down
                    End If
                Case Keys.Left
                    If Not cMovement2.Direction = Movement.intDirection.Right Then
                        cMovement2.Direction = Movement.intDirection.Left
                    End If
                Case Keys.Up
                    If Not cMovement2.Direction = Movement.intDirection.Down Then
                        cMovement2.Direction = Movement.intDirection.Up
                    End If
            End Select

        Else
            Select Case e.KeyCode
                Case Keys.Right
                    If Not cMovement1.Direction = Movement.intDirection.Left Then
                        cMovement1.Direction = Movement.intDirection.Right
                    End If
                Case Keys.Down
                    If Not cMovement1.Direction = Movement.intDirection.Up Then
                        cMovement1.Direction = Movement.intDirection.Down
                    End If
                Case Keys.Left
                    If Not cMovement1.Direction = Movement.intDirection.Right Then
                        cMovement1.Direction = Movement.intDirection.Left
                    End If
                Case Keys.Up
                    If Not cMovement1.Direction = Movement.intDirection.Down Then
                        cMovement1.Direction = Movement.intDirection.Up
                    End If
            End Select
        End If
    End Sub

    Public Sub AutoPlayOrdi()
        If Not isDead Then
            'x
            If cMovement1.Location.X > rectFood.X Then
                If (cMovement1.Direction = Movement.intDirection.Up Or cMovement1.Direction = Movement.intDirection.Down) Then
                    If Not cMovement1.Direction = Movement.intDirection.Right Then
                        cMovement1.Direction = Movement.intDirection.Left
                    End If
                End If

            ElseIf cMovement1.Location.X < rectFood.X Then
                If (cMovement1.Direction = Movement.intDirection.Up Or cMovement1.Direction = Movement.intDirection.Down) Then
                    If Not cMovement1.Direction = Movement.intDirection.Left Then
                        cMovement1.Direction = Movement.intDirection.Right
                    End If
                End If
            End If


            'y
            If cMovement1.Location.Y > rectFood.Y AndAlso _
            (cMovement1.Direction = Movement.intDirection.Left Or cMovement1.Direction = Movement.intDirection.Right) Then
                If Not cMovement1.Direction = Movement.intDirection.Down Then
                    cMovement1.Direction = Movement.intDirection.Up
                End If
            ElseIf cMovement1.Location.Y > rectFood.Y AndAlso cMovement1.Direction = Movement.intDirection.Down Then
                If Not cMovement1.Direction = Movement.intDirection.Right Then
                    cMovement1.Direction = Movement.intDirection.Left
                End If
            End If

            If cMovement1.Location.Y < rectFood.Y AndAlso _
            (cMovement1.Direction = Movement.intDirection.Left Or cMovement1.Direction = Movement.intDirection.Right) Then
                If Not cMovement1.Direction = Movement.intDirection.Up Then
                    cMovement1.Direction = Movement.intDirection.Down
                End If
            End If
        End If
    End Sub

    Public Function MoveTo(ByVal direction As String) As String Implements onRecieveDataListener.MoveTo
        ' snake 2
        Select Case Integer.Parse(direction)
            Case 1
                If Not cMovement2.Direction = Movement.intDirection.Down Then
                    cMovement2.Direction = Movement.intDirection.Up
                End If
            Case 2
                If Not cMovement2.Direction = Movement.intDirection.Right Then
                    cMovement2.Direction = Movement.intDirection.Left
                End If
            Case 3
                If Not cMovement2.Direction = Movement.intDirection.Left Then
                    cMovement2.Direction = Movement.intDirection.Right
                End If
            Case 4
                If Not cMovement2.Direction = Movement.intDirection.Up Then
                    cMovement2.Direction = Movement.intDirection.Down
                End If
        End Select
        Return ""
    End Function

    Private Sub Game_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Initialize()
        p1_name.Text = My.Settings.player1_name
        p2_name.Text = My.Settings.player2_name
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        AutoPlayOrdi()
    End Sub

    Private Sub surfaceSnake_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles surfaceSnake.Click

    End Sub
End Class