Imports System.Net
Imports System.IO
Imports System.Threading
Public Class Form1
    Dim thread As Thread
    Dim f2 As Game = New Game
    Shared listener As onRecieveDataListener = Game
    Shared isConnected As Boolean = False

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        thread = New Thread(AddressOf StartListener)
        thread.Start()
        Timer1.Start()
        TCP.Show()
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If thread.IsAlive Then
                thread.Abort()
            End If
        Catch ex As NullReferenceException
        End Try
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'My.Computer.Audio.Play(My.Resources.dragon, AudioPlayMode.BackgroundLoop)
    End Sub
    Public Shared Sub StartListener()
        Try
            ' Set the TcpListener on port 1995.
            Dim port As Int32 = 1995
            Dim server As New Sockets.TcpListener(port)

            ' Start listening for client requests.
            server.Start()

            ' Buffer for reading data
            Dim bytes(1024) As [Byte]
            Dim data As [String] = Nothing

            ' Enter the listening loop.
            While True
                Console.WriteLine("Waiting for a connection... ")
                ' Perform a blocking call to accept requests.
                ' You could also user server.AcceptSocket() here.
                Dim client As Sockets.TcpClient = server.AcceptTcpClient()
                Console.WriteLine("Connected!")
                isConnected = True
                data = Nothing

                ' Get a stream object for reading and writing
                Dim stream As Sockets.NetworkStream = client.GetStream()
                Dim i As Int32

                ' Loop to receive all the data sent by the client.
                i = stream.Read(bytes, 0, bytes.Length)
                While (i <> 0)
                    ' Translate data bytes to a ASCII string.
                    data = System.Text.Encoding.ASCII.GetString(bytes, 0, i)
                    ' Process the data sent by the client.
                    data = data.ToUpper()
                    ' pass data to remote control
                    listener.MoveTo(data.ToString)
                    'Dim msg As [Byte]() = System.Text.Encoding.ASCII.GetBytes(data)
                    ' Send back a response.
                    'stream.Write(msg, 0, msg.Length)
                    'Console.WriteLine([String].Format("Sent: {0}", data))
                    i = stream.Read(bytes, 0, bytes.Length)
                End While
                ' Shutdown and end connection
                client.Close()
            End While
        Catch e As Sockets.SocketException
            Console.WriteLine("SocketException: {0}", e)
        End Try

        Console.WriteLine(ControlChars.Cr + "Hit enter to continue...")
        Console.Read()
    End Sub 'Listener

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If isConnected Then
            Button3.Enabled = True
            Timer1.Stop()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Game.Show()
        Me.Hide()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Settings.Show()
    End Sub
End Class
