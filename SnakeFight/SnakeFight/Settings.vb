Public Class Settings

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ColorDialog1.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            Button1.BackColor = ColorDialog1.Color
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If ColorDialog1.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            Button2.BackColor = ColorDialog1.Color
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If CheckBox1.Checked Then
            If TextBox2.Text.Length > 0 Then
                My.Settings.player1_name = "Mister Robot"
                My.Settings.player2_name = TextBox2.Text
                My.Settings.player1_color = Button1.BackColor
                My.Settings.player2_color = Button2.BackColor
                My.Settings.isP1_computer = True
                My.Settings.Save()
                Me.Close()
            Else
                MsgBox("Player name cannot be empty !", MsgBoxStyle.Exclamation, "Error Settings")
            End If
        Else
            If TextBox1.Text.Length > 0 AndAlso TextBox2.Text.Length > 0 Then
                My.Settings.player1_name = TextBox1.Text
                My.Settings.player2_name = TextBox2.Text
                My.Settings.player1_color = Button1.BackColor
                My.Settings.player2_color = Button2.BackColor
                My.Settings.isP1_computer = False
                My.Settings.Save()
                Me.Close()
            Else
                MsgBox("Player name cannot be empty !", MsgBoxStyle.Exclamation, "Error Settings")
            End If
        End If
    End Sub

    Private Sub Settings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Button1.BackColor = My.Settings.player1_color
        Button2.BackColor = My.Settings.player2_color
        If My.Settings.player1_name.Equals("Mister Robot") Then
            CheckBox1.Checked = True
        End If
        TextBox1.Text = My.Settings.player1_name
        TextBox2.Text = My.Settings.player2_name

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        TextBox1.Enabled = CheckBox1.CheckState - 1
    End Sub
End Class