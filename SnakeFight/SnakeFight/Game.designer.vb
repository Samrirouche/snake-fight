<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Game
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Game))
        Me.lblMessage = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.surfaceSnake = New System.Windows.Forms.PictureBox
        Me.p1_name = New System.Windows.Forms.Label
        Me.p2_name = New System.Windows.Forms.Label
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.surfaceSnake, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMessage
        '
        Me.lblMessage.BackColor = System.Drawing.Color.DimGray
        Me.lblMessage.Font = New System.Drawing.Font("Segoe Print", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessage.ForeColor = System.Drawing.Color.White
        Me.lblMessage.Location = New System.Drawing.Point(148, 214)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(301, 72)
        Me.lblMessage.TabIndex = 11
        Me.lblMessage.Text = "Press Enter to Start"
        Me.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Timer1
        '
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe Print", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Gray
        Me.Label1.Location = New System.Drawing.Point(149, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 33)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe Print", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Gray
        Me.Label2.Location = New System.Drawing.Point(511, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 33)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "0"
        '
        'Timer2
        '
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.SnakeFight.My.Resources.Resources.snake_head_10_1104000
        Me.PictureBox1.Location = New System.Drawing.Point(263, 1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(59, 34)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 14
        Me.PictureBox1.TabStop = False
        '
        'surfaceSnake
        '
        Me.surfaceSnake.BackColor = System.Drawing.Color.Gray
        Me.surfaceSnake.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.surfaceSnake.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.surfaceSnake.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.surfaceSnake.Location = New System.Drawing.Point(0, 40)
        Me.surfaceSnake.Name = "surfaceSnake"
        Me.surfaceSnake.Size = New System.Drawing.Size(624, 432)
        Me.surfaceSnake.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.surfaceSnake.TabIndex = 10
        Me.surfaceSnake.TabStop = False
        '
        'p1_name
        '
        Me.p1_name.AutoSize = True
        Me.p1_name.Font = New System.Drawing.Font("Segoe Print", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.p1_name.ForeColor = System.Drawing.Color.DarkGray
        Me.p1_name.Location = New System.Drawing.Point(52, 4)
        Me.p1_name.Name = "p1_name"
        Me.p1_name.Size = New System.Drawing.Size(97, 33)
        Me.p1_name.TabIndex = 15
        Me.p1_name.Text = "Player 1"
        '
        'p2_name
        '
        Me.p2_name.AutoSize = True
        Me.p2_name.Font = New System.Drawing.Font("Segoe Print", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.p2_name.ForeColor = System.Drawing.Color.Silver
        Me.p2_name.Location = New System.Drawing.Point(415, 2)
        Me.p2_name.Name = "p2_name"
        Me.p2_name.Size = New System.Drawing.Size(97, 33)
        Me.p2_name.TabIndex = 16
        Me.p2_name.Text = "Player 2"
        '
        'Game
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(624, 472)
        Me.Controls.Add(Me.p2_name)
        Me.Controls.Add(Me.p1_name)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.surfaceSnake)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Game"
        Me.Text = "Game"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.surfaceSnake, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents surfaceSnake As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents p1_name As System.Windows.Forms.Label
    Friend WithEvents p2_name As System.Windows.Forms.Label
End Class
