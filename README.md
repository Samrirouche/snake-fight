# Snake Fight Multiplayer
This game allows you to play either against the computer or against your friend.  
you can choose your opponent and modify everything from the parameters.  

![](SnakeFight/SnakeFight/Resources/home.png)

# Game & Control
the game window is as you can see in the picture at the bottom.
![](SnakeFight/SnakeFight/Resources/game.png)

* When we choose to play against the computer (Simon),  
 we can control the player 2 from keyboard arrows.  
 ↑ UP  
 ↓ DOWN  
 → RIGHT  
 ← LEFT  
 SPACE to PAUSE GAME  
 ENTER to Start or Resume.  
* In case of player1 VS player2, the player1 should be controlled by an Android application by TCP.  
- First we click on "Connexion" button to start Server TCP.
- then, we connect our application client to the server and we can then control the directions by this application.  

# Settings
We can customize the colors of the players as well as the names and the type of players (computer/player).  
![](SnakeFight/SnakeFight/Resources/settings.png)

# Conclusion 
It is true that this game appears simple but, a lot of calculations were used for the control of the directions  
 and the analysis of the behavior of each snake.
 So, it allowed me to improvise my knowledge in .NET and to understand the control   
 mechanisms and finally, of course to see the difficulties of this kind of games.


